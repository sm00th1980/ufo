//
//  YCGeoParser.m
//  ufo
//
//  Created by Deyarov Ruslan on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "YCGeoParser.h"

@interface YCGeoParserTests : XCTestCase
@property (strong, nonatomic) NSArray *valid_geos;
@property (strong, nonatomic) NSArray *invalid_geos;

@end

@implementation YCGeoParserTests

- (void)setUp
{
    [super setUp];
    
    _valid_geos = @[
                    @{
                        @"in": @"  Face: 39°26'24.76\"N 84°17'34.93\"W",
                        @"out": @{
                                @"latitude": @{@"degrees": @"39", @"minutes": @"26", @"seconds": @"24.76"},
                                @"longitude": @{@"degrees": @"84", @"minutes": @"17", @"seconds": @"34.93"}
                                }
                        },
                    @{
                        @"in": @"  Ufo 1: 45°42'12.73\"N 21°18'7.53\"E",
                        @"out": @{
                                @"latitude": @{@"degrees": @"45", @"minutes": @"42", @"seconds": @"12.73"},
                                @"longitude": @{@"degrees": @"21", @"minutes": @"18", @"seconds": @"7.53"}
                                }
                        },
                    @{
                        @"in": @"  33°44'17.45\"N 112°38'0.17\"W Massive Triangle carved into mountains",
                        @"out": @{
                                @"latitude": @{@"degrees": @"33", @"minutes": @"44", @"seconds": @"17.45"},
                                @"longitude": @{@"degrees": @"112", @"minutes": @"38", @"seconds": @"0.17"}
                                }
                        },
                    @{
                        @"in": @"  30°30'38.82\"S 115°22'56.14\"E Black Triangular UFO",
                        @"out": @{
                                @"latitude": @{@"degrees": @"30", @"minutes": @"30", @"seconds": @"38.82"},
                                @"longitude": @{@"degrees": @"115", @"minutes": @"22", @"seconds": @"56.14"}
                                }
                        },
                    @{
                        @"in": @"  Antarctic Expedition building 1 - 71°40'22.60\"S 2°50'20.95\"W",
                        @"out": @{
                                @"latitude": @{@"degrees": @"71", @"minutes": @"40", @"seconds": @"22.60"},
                                @"longitude": @{@"degrees": @"2", @"minutes": @"50", @"seconds": @"20.95"}
                                }
                        },
                    @{
                        @"in": @" (Coordinates: 34°21’12.33″S 18°29’24.02″E).",
                        @"out": @{
                                @"latitude": @{@"degrees": @"34", @"minutes": @"21", @"seconds": @"12.33"},
                                @"longitude": @{@"degrees": @"18", @"minutes": @"29", @"seconds": @"24.02"}
                                }
                        },
                    @{
                        @"in": @" spotted on Google Earth. The video featured live capture of the Google Earth search and zooming in until you are able to see the UFO. This footage was discovered in Google Earth by searching to South Africa. Check it out by yourself on Google Earth! Here are the coordinates reported on this video: (Coordinates: 34°21’12.33″S 18°29’24.02″E).",
                        @"out": @{
                                @"latitude": @{@"degrees": @"34", @"minutes": @"21", @"seconds": @"12.33"},
                                @"longitude": @{@"degrees": @"18", @"minutes": @"29", @"seconds": @"24.02"}
                                }
                        },
                    @{
                        @"in": @"<div class=\"separator\" style=\"clear: both; text-align: center;\"><a href=\"http://1.bp.blogspot.com/--FFDjVs-6YI/UU0JvishZHI/AAAAAAAADUA/-ijA1H6ScXE/s1600/UFOs+in+google+Earth+2013.jpg\" imageanchor=\"1\" style=\"clear: right; float: right; margin-bottom: 1em; margin-left: 1em;\"><img border=\"0\" height=\"150\" src=\"http://1.bp.blogspot.com/--FFDjVs-6YI/UU0JvishZHI/AAAAAAAADUA/-ijA1H6ScXE/s200/UFOs+in+google+Earth+2013.jpg\" width=\"200\" /></a></div>Here are the 3 most important UFOs found in Google Earth including a detailed explanation of each UFO.<br /><br />UFO 1 :<br />Localization : West Oceania<br />Coordinates : 30°30'38.86\"S 115°22'58.17\"E<br />",
                        @"out": @{
                                @"latitude": @{@"degrees": @"30", @"minutes": @"30", @"seconds": @"38.86"},
                                @"longitude": @{@"degrees": @"115", @"minutes": @"22", @"seconds": @"58.17"}
                                }
                        },
                    @{
                        @"in": @"<span id=\"video_title\" class=\"ellipsis pull-start font-xxl  clickable\" title=\"ufo G.E. Latitude: 30°30'38.82&quot;S Longitude:115°22'56.14&quot;E\">ufo G.E. Latitude: 30°30'38.82\"S Longitude:115°22'56.14\"E</span>\"",
                        @"out": @{
                                @"latitude": @{@"degrees": @"30", @"minutes": @"30", @"seconds": @"38.82"},
                                @"longitude": @{@"degrees": @"115", @"minutes": @"22", @"seconds": @"56.14"}
                                }
                        },
                    @{
                        @"in": @"<tr><td><a href=\"#UFOorb12\" class=\"style16\">UFO Orb 1-2 </a></td><td> 26°45'0.61\" N </td><td> 80°11'21.55\" W </td><td>&nbsp;</td><td>Orb</td><td><font color=\"#ffff66\">»</font> <a href=\"http://maps.google.com/maps?ll=26.748651,-80.189370&amp;spn=0.0,0.0&amp;t=k\" target=\"_blank\" class=\"style16\">place on map <font color=\"#ffff66\"></font></a></td></tr>\"",
                        @"out": @{
                                @"latitude": @{@"degrees": @"26", @"minutes": @"45", @"seconds": @"0.61"},
                                @"longitude": @{@"degrees": @"80", @"minutes": @"11", @"seconds": @"21.55"}
                                }
                        },
                    @{
                        @"in": @"<tr>&#13;<td><a href=\"#UFOorb12\" class=\"style16\">UFO Orb 1-2 </a></td>&#13;<td> 27°45'0.61\" N </td>&#13;<td> 80°11'21.55\" W </td>&#13;<td> </td>&#13;<td>Orb</td>&#13;<td><font color=\"#ffff66\">»</font> <a href=\"http://maps.google.com/maps?ll=26.748651,-80.189370&amp;spn=0.0,0.0&amp;t=k\" target=\"_blank\" class=\"style16\">place on map <font color=\"#ffff66\"/></a></td>&#13;</tr>\"",
                        @"out": @{
                                @"latitude": @{@"degrees": @"27", @"minutes": @"45", @"seconds": @"0.61"},
                                @"longitude": @{@"degrees": @"80", @"minutes": @"11", @"seconds": @"21.55"}
                                }
                        }
                    ];
    
    _invalid_geos = @[@"", @"-1", @123, [NSNull null]];
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testShouldCorrectParseValidGeos
{
    for (NSDictionary *valid_geo in _valid_geos) {
        NSDictionary *coordinate = [YCGeoParser parse:valid_geo[@"in"]];
        XCTAssertEqualObjects(valid_geo[@"out"], coordinate);
    }
}

- (void)testShouldReturnNilForInvalidGeos
{
    for (NSString *invalid_geo in _invalid_geos) {
        XCTAssertNil([YCGeoParser parse:invalid_geo]);
    }
}

@end
