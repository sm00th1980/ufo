//
//  YCGeoConverterTests.m
//  ufo
//
//  Created by Deyarov Ruslan on 15.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "YCGeoConverter.h"

@interface YCGeoConverterTests : XCTestCase
@property (strong, nonatomic) NSArray *valid_coordinates;
@end

@implementation YCGeoConverterTests

- (void)setUp
{
    [super setUp];
    
    _valid_coordinates = @[
                    @{
                        @"degrees": @"30", @"minutes": @"30", @"seconds": @"38.82",
                        @"out": @30.51078
                        },
                    @{
                        @"degrees": @"115", @"minutes": @"22", @"seconds": @"56.14",
                        @"out": @115.38226
                        },
                    @{
                        @"degrees": @"29", @"minutes": @"51", @"seconds": @"57.76",
                        @"out": @29.8660444
                        },
                    @{
                        @"degrees": @"31", @"minutes": @"12", @"seconds": @"57.51",
                        @"out": @31.2159750
                        }
                    ];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testShouldConvertValidCoordinate
{
    for (NSDictionary *valid_coordinate in _valid_coordinates) {
        NSNumber *coordinate = [YCGeoConverter convert:valid_coordinate[@"degrees"] minutes:valid_coordinate[@"minutes"] seconds:valid_coordinate[@"seconds"]];
        XCTAssertEqualWithAccuracy([valid_coordinate[@"out"] doubleValue], [coordinate doubleValue], 0.0001);
    }
}

@end
