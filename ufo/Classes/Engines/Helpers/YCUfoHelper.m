//
//  YCUfoHelper.m
//  ufo
//
//  Created by Deyarov Ruslan on 20.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCUfoHelper.h"
#import "YCGeoParser.h"
#import "YCGeoConverter.h"
#import "YCUfo.h"
#import "GDataXMLNode.h"

@implementation YCUfoHelper

+ (NSArray*)parseCoordinates:(NSData*)HTMLData
{
    NSMutableSet *coordinates = [NSMutableSet set];
    
    GDataXMLDocument *doc = [[GDataXMLDocument alloc]initWithHTMLData:HTMLData error:NULL];
    NSArray *nodes = [doc nodesForXPath:@"//*[text()]" error:NULL];
    
    NSDictionary *coordinate = nil;
    for (GDataXMLElement *node in nodes) {
        @try {
            coordinate = [YCGeoParser parse:[node XMLString]];
        }
        
        @catch ( NSException *e ) {}
        
        NSNumber *latitude = [YCGeoConverter convert:coordinate[@"latitude"][@"degrees"] minutes:coordinate[@"latitude"][@"minutes"] seconds:coordinate[@"latitude"][@"seconds"]];
        NSNumber *longitude = [YCGeoConverter convert:coordinate[@"longitude"][@"degrees"] minutes:coordinate[@"longitude"][@"minutes"] seconds:coordinate[@"longitude"][@"seconds"]];
        
        //exclude zeros coordinates
        if (![latitude floatValue] == 0.0f || ![longitude floatValue] == 0.0f) {
            [coordinates addObject:@{@"latitude": latitude, @"longitude": longitude}];
        }
    } //for
    
    return [coordinates allObjects];
}

@end
