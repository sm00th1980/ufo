//
//  YCPageEngine.m
//  ufo
//
//  Created by Deyarov Ruslan on 20.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCUfoEngine.h"
#import "YCUfoHelper.h"
#import "YCUfo.h"

@implementation YCUfoEngine

#pragma mark - Public Interface
+ (void)load:(NSString*)URLString success:(void_block_id)success failure:(void_block_string)failure
{
    void (^successCompletion)(id response) = ^(id response) {
        success(response);
    };
    
    [[self new] fetch:URLString success:successCompletion failure:failure];
}

+ (void)parse:(id)response success:(void_block_array)success failure:(void_block_string)failure
{
    __block NSArray *coordinates;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        coordinates = [YCUfoHelper parseCoordinates:response];

        dispatch_async(dispatch_get_main_queue(), ^(void){
            //saving new coordinates
            NSMutableArray *ufos = [NSMutableArray array];

            if (coordinates.count > 0) {
                //destroy all old coordinates
                [YCUfo removeAll];
                for (NSDictionary *coordinate in coordinates) {
                    YCUfo *ufo = [YCUfo createByLatitude:coordinate[@"latitude"] longitude:coordinate[@"longitude"]];
                    if (ufo) {
                        [ufos addObject:ufo];
                    }
                }
            }

            //Run UI Updates
            if (ufos.count > 0) {
                success([ufos copy]);
            } else {
                failure(NSLocalizedString(@"Valid coordinates were not found on this page", nil));
            }
        });

    });
}

@end
