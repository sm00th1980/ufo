//
//  YCPageEngine.h
//  ufo
//
//  Created by Deyarov Ruslan on 20.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCBaseEngine.h"

@interface YCUfoEngine : YCBaseEngine

+ (void)load:(NSString*)URLString success:(void_block_id)success failure:(void_block_string)failure;
+ (void)parse:(id)response success:(void_block_array)success failure:(void_block_string)failure;
@end
