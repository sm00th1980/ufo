//
//  YCGeoParser.h
//  ufo
//
//  Created by Deyarov Ruslan on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCGeoParser : NSObject
+ (NSDictionary*)parse:(NSString*)string;
@end
