//
//  YCGeoParser.m
//  ufo
//
//  Created by Deyarov Ruslan on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

typedef enum {
    UNKNOWN,
    NW,
    SW,
    NE,
    SE
} Patterns;

#import "YCGeoParser.h"

@implementation YCGeoParser

+ (NSDictionary*)parse:(NSString*)string
{
    if (UNKNOWN != [self findPattern:string]) {
        NSString *latitude_ = nil;
        NSString *longitude_ = nil;
        
        if (NW == [self findPattern:string]) {
            latitude_ = [self parseString:string withCharacter:@"N"];
            longitude_  = [self parseString:string withCharacter:@"W"];
        }
        
        if (SW == [self findPattern:string]) {
            latitude_ = [self parseString:string withCharacter:@"S"];
            longitude_  = [self parseString:string withCharacter:@"W"];
        }
        
        if (NE == [self findPattern:string]) {
            latitude_ = [self parseString:string withCharacter:@"N"];
            longitude_  = [self parseString:string withCharacter:@"E"];
        }
        
        if (SE == [self findPattern:string]) {
            latitude_ = [self parseString:string withCharacter:@"S"];
            longitude_  = [self parseString:string withCharacter:@"E"];
        }
        
        if (latitude_ && longitude_) {
            NSArray *latitude  = [latitude_  componentsSeparatedByString:@" "];
            NSArray *longitude = [longitude_ componentsSeparatedByString:@" "];
            
            return @{
                     @"latitude":  @{@"degrees": latitude[0],  @"minutes": latitude[1],  @"seconds": latitude[2]},
                     @"longitude": @{@"degrees": longitude[0], @"minutes": longitude[1], @"seconds": longitude[2]}
                     };
            
        }
    }
    
    return nil;
}

+ (Patterns)findPattern:(NSString*)string
{
    if ([string isKindOfClass:[NSString class]]) {
        for (NSString *second_quote in [self second_quotes]) {
            NSArray *patterns = @[
                                  @{@"index": [NSNumber numberWithInteger:NW], @"string": [NSString stringWithFormat:@"%@N.{1,}%@W", second_quote, second_quote] },
                                  @{@"index": [NSNumber numberWithInteger:SW], @"string": [NSString stringWithFormat:@"%@S.{1,}%@W", second_quote, second_quote] },
                                  @{@"index": [NSNumber numberWithInteger:NE], @"string": [NSString stringWithFormat:@"%@N.{1,}%@E", second_quote, second_quote] },
                                  @{@"index": [NSNumber numberWithInteger:SE], @"string": [NSString stringWithFormat:@"%@S.{1,}%@E", second_quote, second_quote] }
                                  ];
            
            for (NSDictionary *pattern in patterns) {
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern[@"string"] options:NSRegularExpressionCaseInsensitive error:nil];
                NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
                
                if (!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) {
                    return [pattern[@"index"] integerValue];
                }
            } //for
        } //for second quotes
    } //if
    
    return UNKNOWN;
}

+ (NSString*)parseString:(NSString*)before_string withCharacter:(NSString*)character
{
    NSString *after_string = nil;
    
    for (NSString *minute_quote in [self minute_quotes]) {
        for (NSString *second_quote in [self second_quotes]) {
            NSString *pattern = [NSString stringWithFormat:@"\\d{1,}°\\d{1,}%@\\d{1,}.\\d{1,}%@%@", minute_quote, second_quote, character];
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
            
            NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:before_string options:0 range:NSMakeRange(0, before_string.length)];
            
            if (!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0))) {
                after_string = [self cleanString:[before_string substringWithRange:rangeOfFirstMatch]];
            }
        } //for
    } //for
    
    return after_string;
}

#pragma mark - cleaning

+ (NSString*)cleanString:(NSString*)string
{
    NSString *after_string = string;
    
    after_string = [self removeSpecialCharacter:after_string character:@"°" replacement:@" "];
    
    for (NSString *minute_quote in [self minute_quotes]) {
        after_string = [self removeSpecialCharacter:after_string character:minute_quote replacement:@" "];
    }
    
    for (NSString *second_quote in [self second_quotes]) {
        after_string = [self removeSpecialCharacter:after_string character:[NSString stringWithFormat:@"%@N", second_quote] replacement:@" "];
        after_string = [self removeSpecialCharacter:after_string character:[NSString stringWithFormat:@"%@S", second_quote] replacement:@" "];
        
        after_string = [self removeSpecialCharacter:after_string character:[NSString stringWithFormat:@"%@W", second_quote] replacement:@""];
        after_string = [self removeSpecialCharacter:after_string character:[NSString stringWithFormat:@"%@E", second_quote] replacement:@""];
    }
    
    return after_string;
}

+ (NSString*)removeSpecialCharacter:(NSString*)string character:(NSString*)character replacement:(NSString*)replacement
{
    return [string stringByReplacingOccurrencesOfString:character withString:replacement];
}

+ (NSArray*)second_quotes
{
    return @[@"\"", @"″", @"\" ",  @"″ "];
}

+ (NSArray*)minute_quotes
{
    return @[@"’", @"'", @"’ ", @"' "];
}

@end
