//
//  YCGeoConverter.h
//  ufo
//
//  Created by Deyarov Ruslan on 15.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YCGeoConverter : NSObject
+ (NSNumber*)convert:(NSString*)degrees minutes:(NSString*)minutes seconds:(NSString*)seconds;
@end
