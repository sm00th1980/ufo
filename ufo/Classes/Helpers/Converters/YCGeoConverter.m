//
//  YCGeoConverter.m
//  ufo
//
//  Created by Deyarov Ruslan on 15.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCGeoConverter.h"

@implementation YCGeoConverter

+ (NSNumber*)convert:(NSString*)degrees minutes:(NSString*)minutes seconds:(NSString*)seconds
{
    int latsign = 1;
    double degree = [degrees doubleValue];
    double minute = [minutes doubleValue];
    double second = [seconds doubleValue];
    
    if (degree < 0) {
        latsign = -1;
    } else {
        latsign = 1;
    }
    
    return [NSNumber numberWithDouble:(degree + (latsign * (minute/60.)) + (latsign * (second/3600.)))];
}

@end
