//
//  YCApplicationData.h
//  ufo
//
//  Created by Deyarov Ruslan on 20.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface YCApplicationData : NSObject <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocation *currentLocation;

+ (instancetype)sharedInstance;

+ (NSString*)applicationName;
+ (NSString*)applicationVersion;

+ (NSDictionary*)customHeaders;
@end
