//
//  YCApplicationData.m
//  ufo
//
//  Created by Deyarov Ruslan on 20.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCApplicationData.h"

@interface YCApplicationData ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation YCApplicationData

#pragma mark - publice interface
+ (NSString*)applicationVersion
{
    //application version
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [NSString stringWithFormat:@"%@", [infoDictionary objectForKey:@"CFBundleShortVersionString"]];
}

+ (NSString*)applicationName
{
    //application name
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    return [NSString stringWithFormat:@"%@", [infoDictionary objectForKey:@"CFBundleDisplayName"]];
}

+ (NSDictionary*)customHeaders
{
    return @{
             @"x-client-identifier" : @"iOS",
             @"x-client-appname" : [self applicationName],
             @"x-client-version" : [self applicationVersion]
             };
}

#pragma mark - Singleton
+ (instancetype)sharedInstance
{
    static YCApplicationData *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[YCApplicationData alloc] initWithLocationManager];
    });
    
    return sharedInstance;
}

#pragma mark - CLLocationManager
- (instancetype)initWithLocationManager
{
    if(self = [super init]) {
        self.locationManager = [[CLLocationManager alloc] init];
        
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        [_locationManager startUpdatingLocation];
    }
    return self;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)location
{
    _currentLocation = [location lastObject];
}

@end