//
//  Ufo.h
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DCModel.h"
#import "YCBlocks.h"
#import <CoreLocation/CoreLocation.h>

@interface YCUfo : NSManagedObject

@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * latitude;

@property (nonatomic, strong) NSMutableArray * errors;

+ (instancetype)createByLatitude:(NSNumber*)latitude longitude:(NSNumber*)longitude;
+ (void)removeAll;
- (void)remove;

- (NSString *)deg_latitude;
- (NSString *)deg_longitude;
- (CLLocationDistance)distance;
- (double)direction;
@end
