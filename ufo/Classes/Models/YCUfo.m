//
//  Ufo.m
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#define MIN_LATITUDE  -90.0f
#define MAX_LATITUDE  90.0f
#define MIN_LONGITUDE -180.0f
#define MAX_LONGITUDE 180.0f

double DegreesToRadians(double degrees) {return degrees * M_PI / 180;};
double RadiansToDegrees(double radians) {return radians * 180/M_PI;};

#import "YCUfo.h"


@implementation YCUfo

@synthesize errors = _errors;

@dynamic longitude;
@dynamic latitude;

#pragma mark - public interface

+ (instancetype)createByLatitude:(NSNumber*)latitude longitude:(NSNumber*)longitude
{
    YCUfo *new_ufo = [YCUfo newObject];
    new_ufo.latitude = latitude;
    new_ufo.longitude = longitude;
    
    if ([new_ufo isValid]) {
        [new_ufo save]; //saving object
        
        //return valid object
        return new_ufo;
    } else {
        //validation failed - not saving object
        return nil;
    }
}

+ (void)removeAll
{
    [self all:^(id ufos){
        [self destroyObjects:ufos success:^(void){} failure:^(NSError *error){}];
    }];
}

- (void)remove
{
    [YCUfo destroyObject:self success:^(void){} failure:^(NSError *error){}];
}

- (NSString *)deg_latitude
{
    return [self degrees:self.latitude latitude:YES];
}

- (NSString *)deg_longitude
{
    return [self degrees:self.longitude latitude:NO];
}

- (CLLocationDistance)distance
{
    return [[[YCApplicationData sharedInstance] currentLocation] distanceFromLocation:[[CLLocation alloc] initWithLatitude:[self.latitude doubleValue] longitude:[self.longitude doubleValue]]];
}

- (double)direction
{
    CLLocation *currentLocation = [[YCApplicationData sharedInstance] currentLocation];
    
    double lat1 = DegreesToRadians(currentLocation.coordinate.latitude);
    double lon1 = DegreesToRadians(currentLocation.coordinate.longitude);
    
    double lat2 = DegreesToRadians([self.latitude doubleValue]);
    double lon2 = DegreesToRadians([self.longitude doubleValue]);
    
    double dLon = lon2 - lon1;
    
    double y = sin(dLon) * cos(lat2);
    double x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
    double radiansBearing = atan2(y, x);
    
    return RadiansToDegrees(radiansBearing);
}

#pragma mark - private interface

- (NSString*)degrees:(NSNumber*)position latitude:(BOOL)is_latitude
{
    double position_ = [position doubleValue];
    
    int seconds = abs(round(position_ * 3600));
    int degrees = seconds / 3600;
    seconds = abs(seconds % 3600);
    int minutes = seconds / 60;
    float s = fmod(fmod(position_*3600, 3600), 60);
    
    //N,E,S,W notation in lat/long
    NSString *direction = @"";
    if (is_latitude) {
        direction = (position_ >= 0) ? @"N" : @"S";
    } else {
        direction = (position_ >= 0) ? @"E" : @"W";
    }
    
    return [NSString stringWithFormat:@"%i° %i' %.2f\"%@", degrees, minutes, s, direction];
}

- (void)save
{
    [self save:^(id items){} failure:^(NSError *error){}];
}

- (BOOL)isValid
{
    //validation
    //1)latitude should be in -90..+90
    //2)longitude should be in -180..+180
    //3)latitidute and longitude should not be equal 0 both
    if (_errors) {
        [_errors removeAllObjects];
    } else {
        _errors = [NSMutableArray array];
    }
    
    //check latitude
    if ([self.latitude doubleValue] < MIN_LATITUDE || [self.latitude doubleValue] > MAX_LATITUDE) {
        NSString *error_str = [NSString stringWithFormat:@"latitude should be in %.2f..%.2f", MIN_LATITUDE, MAX_LATITUDE];
        [_errors addObject:NSLocalizedString(error_str, nil)];
    }
    
    //check longitude
    if ([self.longitude doubleValue] < MIN_LONGITUDE || [self.longitude doubleValue] > MAX_LONGITUDE) {
        NSString *error_str = [NSString stringWithFormat:@"longitude should be in %.2f..%.2f", MIN_LONGITUDE, MAX_LONGITUDE];
        [_errors addObject:NSLocalizedString(error_str, nil)];
    }
    
    //check latitude and longitude on zeros
    if ([self.latitude doubleValue] == 0.0f && [self.longitude doubleValue] == 0.0f) {
        NSString *error_str = [NSString stringWithFormat:@"latitude longitude should not be zeros both"];
        [_errors addObject:NSLocalizedString(error_str, nil)];
    }

    if ([_errors count] > 0) {
        return NO;
    }
    
    return YES;
}


@end
