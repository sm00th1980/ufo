//
//  YCBlocks.h
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

typedef void (^void_block_string)(NSString*);
typedef void (^void_block_id)(id);
typedef void (^void_block_array)(NSArray*);
typedef void (^void_block_bool_arr)(BOOL success, NSArray *errors);
typedef void (^void_block_void)(void);