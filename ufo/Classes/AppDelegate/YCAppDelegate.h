//
//  YCAppDelegate.h
//  ufo
//
//  Created by Deyarov Ruslan on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
