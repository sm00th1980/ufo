//
//  YCUfoListVCDelegate.h
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YCUfoListVCProtocol <NSObject>
@property (strong, nonatomic) NSString *url;
@end
