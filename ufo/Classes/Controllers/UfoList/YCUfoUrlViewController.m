//
//  YCUfoUrlViewController.m
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#define SAMPLE1_URL @"http://voices.yahoo.com/the-google-earth-secret-coordinates-master-collection-2143520.html?cat=2"
#define SAMPLE2_URL @"http://ufosightingshotspot.blogspot.ru/2013/03/the-3-most-important-ufos-in-google.html"

#import "YCUfoUrlViewController.h"

@interface YCUfoUrlViewController ()
@property (weak, nonatomic) IBOutlet UITextView *urlTextView;
@end

@implementation YCUfoUrlViewController

- (IBAction)useCustomUrl:(id)sender
{
    [self close:_urlTextView.text];
}

- (IBAction)useSample1Url:(id)sender
{
    [self close:SAMPLE1_URL];
}

- (IBAction)useSample2Url:(id)sender
{
    [self close:SAMPLE2_URL];
}

- (void)close:(NSString*)url
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^(void){
        [self.delegate setUrl:url];
    }];
}

@end
