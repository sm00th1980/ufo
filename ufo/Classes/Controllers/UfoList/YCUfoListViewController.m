//
//  YCFirstViewController.m
//  ufo
//
//  Created by Деяров Руслан on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#define CELL_IDENTIFIER @"ufo list cell"
#define CELL_HEIGHT 84.0f;
#define TEXT_LABEL_NUMBER_OF_LINES 2
#define DETAIL_TEXT_LABEL_NUMBER_OF_LINES 2

#import "YCUfoListViewController.h"
#import "YCUfoEngine.h"
#import "MBProgressHUD.h"
#import "YCUfo.h"
#import "YCUfoUrlViewController.h"

@interface YCUfoListViewController ()
@property (strong, nonatomic) NSArray* data;
@end

@implementation YCUfoListViewController

- (void)setUrl:(NSString *)url
{
    self->_url = url;
    [self loadData];
}

- (void)loadData
{
    if (_url) {
        [self showHUD:NSLocalizedString(@"Loading...", nil)];
        
        [YCUfoEngine load:_url success:^(id response) {
            
            [self showHUD:NSLocalizedString(@"Parsing...", nil)];
            
            [YCUfoEngine parse:response success:^(NSArray *ufos){
                self.data = ufos;
                [self.tableView reloadData];
                
                [self hideHUD];
            } failure:^(NSString* parsing_error){
                //notify use about error in parsing process
                [self notifyUserAboutError:parsing_error];
            }];
        } failure:^(NSString *loading_error) {
            //notify use about error in loading process
            [self notifyUserAboutError:loading_error];
        }];
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"show url form"]) {
        YCUfoUrlViewController *vc = (YCUfoUrlViewController*)[segue destinationViewController];
        vc.delegate = self;
    }
}

- (void)showHUD:(NSString*)message
{
    [self hideHUD];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableView.superview animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = message;
}

- (void)hideHUD
{
    //hide HUD
    [MBProgressHUD hideHUDForView:self.tableView.superview animated:YES];
}

- (void)notifyUserAboutError:(NSString*)error_message
{
    [self hideHUD];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error occured", nil)
                                                        message:error_message
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil, nil];
    [alertView show];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.data.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    // Configure the cell...
    YCUfo *ufo = (YCUfo*)self.data[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %.2f %@\nDirection: %.2f %@", NSLocalizedString(@"Distance", nil), ufo.distance, NSLocalizedString(@"meters", nil), ufo.direction, NSLocalizedString(@"degrees", nil)];
    cell.textLabel.numberOfLines = TEXT_LABEL_NUMBER_OF_LINES;
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Latitude:%@\nLongitude:%@", ufo.deg_latitude, ufo.deg_longitude];
    cell.detailTextLabel.numberOfLines = DETAIL_TEXT_LABEL_NUMBER_OF_LINES;
    
    return cell;
}

@end
