//
//  YCFirstViewController.h
//  ufo
//
//  Created by Deyarov Ruslan on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCUfoListVCProtocol.h"

@interface YCUfoListViewController : UITableViewController <YCUfoListVCProtocol>
@property (strong, nonatomic) NSString *url;
@end
