//
//  YCUfoUrlViewController.h
//  ufo
//
//  Created by Deyarov Ruslan on 21.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YCUfoListVCProtocol.h"

@interface YCUfoUrlViewController : UIViewController
@property (weak, nonatomic) id<YCUfoListVCProtocol> delegate;
@end
