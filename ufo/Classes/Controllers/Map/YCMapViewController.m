//
//  YCSecondViewController.m
//  ufo
//
//  Created by Деяров Руслан on 14.03.14.
//  Copyright (c) 2014 youct.ru. All rights reserved.
//

#import "YCMapViewController.h"
#import "YCUfo.h"

@interface YCMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation YCMapViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self setupUfos];
}

- (void)setupUfos
{
    //remove previous pins from map
    [self.mapView removeAnnotations:self.mapView.annotations];

    [YCUfo all:^(id ufos){
        [ufos bk_each:^(id el) {
            YCUfo *ufo = (YCUfo*)el;
            
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([ufo.latitude doubleValue], [ufo.longitude doubleValue]);
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:coordinate];
            
            [annotation setTitle:NSLocalizedString(@"Reversing location...", nil)];
            [annotation setSubtitle:[NSString stringWithFormat:@"lat:%@ long:%@", ufo.deg_latitude, ufo.deg_longitude]];

            [self.mapView addAnnotation:annotation];
        }];
    }];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    __weak MKPointAnnotation *pin = (MKPointAnnotation*)view.annotation;
    [self reverseLocation:view.annotation.coordinate.latitude longitude:view.annotation.coordinate.longitude completion:^(NSString *title){
        [pin setTitle:title];
    }];
}

- (void)reverseLocation:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude completion:(void_block_string)completion
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [[CLGeocoder new] reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error){
        if (placemarks.count > 0) {
            completion([[placemarks firstObject] description]);
        } else {
            completion(NSLocalizedString(@"Can't reverse location:(", nil));
        }
    }];
}
@end
